from django.contrib import admin
from bulk_api.models import *


admin.site.register(FacebookPage)
admin.site.register(FacebookLabel)
admin.site.register(Configuration)
