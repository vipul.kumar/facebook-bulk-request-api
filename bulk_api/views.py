from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

from bulk_api.models import *

import csv
import requests
import json


class FacebookBulkRequestAPI(APIView):
    permission_classes = (IsAuthenticated, )

    base_url = "//graph.facebook.com"
    relative_url = "v2.11/{label}/label?access_token={token}"

    user_list = list()
    label_id = None
    access_token = None         # Not sure where this token will come from.
    input_file = None
    request_limit = None

    def post(self, request):
        """
        In case of CSV: Input file is expected supply the user list and label id will be submitted to the API.
        In case of JSON: Input file is expected supply the user list and label id.
        """

        self.input_file = request.FILES.get("file")
        self.request_limit = Configuration.objects.first().request_limit

        if request.version == "v1":
            if self.input_file.name.endswith(".json"):
                file_data = json.loads(self.input_file)
                self.user_list = file_data["users"]
                self.label_id = file_data["label"]

            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)

        if request.version == "v2":
            if self.input_file.name.endswith(".csv"):
                file_data = csv.reader(self.input_file)
                self.user_list = list(file_data)
                self.label_id = request.data.get("label")
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)

        label_data = self.make_label_payload()

        payload = self.make_bulk_payload(data=self.user_list,
                                         url=label_data["url"],
                                         method="POST", access_token=self.access_token)

        response = requests.request("POST", url=self.base_url, data=payload)

        response_code = [item["code"] for item in response]

        success_code = 200

        if all(item == success_code for item in response_code) and len(response_code) != 0:
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def make_label_payload(self):
        url = self.relative_url.format(label=self.label, token=self.access_token)
        url += "?limit=" + self.request_limit

        payload = {
            "user_list": self.user_list,
            "url": url
        }
        return payload

    def make_bulk_payload(self, data=None, url=None, method=None, access_token=None):
        payload = dict()
        payload["access_token"] = access_token
        payload["batch"] = list()

        for item in data:
            payload["batch"].append({"method": method, "relative_url": url, "body": {"user": item}})

        return payload

    # Not clear when to use this function and where the page id will come from. Also what are missing users
    def get_users(self, user_id, page_id):
        response = requests.request("GET", "/get-users/", data={"user_id": user_id, "page_id": page_id})
        return response


class GetUsersAPI(APIView):
    '''
    Fetch user from user_id and page_id

    :param:
    user_id, page_id

    :return:
    list of users if user is not None else return None
    '''
    queryset = User.objects.all()

    def get(self, request):
        # Not sure where to filter user by page id, query would have "page=request.data.get('page_id')" as another filter parameter
        user = User.objects.filter(id=self.request.data.get("user_id"))

        if user is not None:
            return Response(user, status=status.HTTP_200_OK)
        else:
            return Response(None, status=status.HTTP_404_NOT_FOUND)

'''
    To support large number of users we can use a genrator to create user 
    sub-lists from the input list and create payloads accordingly. 
    This would ensure that the max limit of API Calls is not exceeded. 
'''