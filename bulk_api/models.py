from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator


class FacebookPage(models.Model):
    access_token = models.TextField(null=True, blank=False)
    original_id = models.TextField(null=True, blank=False, help_text="Graph ID")


class FacebookLabel(models.Model):
    owner = models.ForeignKey(User, null=True, blank=False)
    page = models.ForeignKey(FacebookPage, null=True, blank=False)
    label_id = models.TextField()


class Configuration(models.Model):
    request_limit = models.IntegerField(null=True, blank=False,
                                        validators=[MinValueValidator(1), MaxValueValidator(50)],
                                        help_text="Value should be in range of 1 to 50")

