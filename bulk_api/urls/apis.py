from django.conf.urls import url
from bulk_api.views import *

urlpatterns = [
    url(r'^get-users/', GetUsersAPI.as_view(), name='get-user-list'),
    url(r'^bulk-request/', FacebookBulkRequestAPI.as_view(), name='bulk-request'),
]