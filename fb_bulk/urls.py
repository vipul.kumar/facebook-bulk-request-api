from django.conf.urls import url, include
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^api/v1/', include('bulk_api.urls.apis', namespace="v1")),
    url(r'^api/v2/', include('bulk_api.urls.apis', namespace="v2")),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
